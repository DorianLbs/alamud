# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class LaunchWithEvent(Event3):
    NAME="launch-with"
    
    def perform(self):
        if not self.object.has_prop("launchable-with"):
            self.fail()
            return self.inform("launch-with.failed")
        if not self.object2.has_prop("launcher"):
            self.fail()
            return self.inform("launch-with.failed")
        self.inform("launch-with")

class LaunchOnEvent(Event2):
    NAME = "launch-on"

    def perform(self):
        if not self.object.has_prop("launchable"):
            self.fail()
            return self.inform("launch-on.failed")
        self.inform("launch-on")


class LaunchOffEvent(Event2):
    NAME = "launch-off"

    def perform(self):
        if not (self.object.has_prop("launchable") or self.object.has_prop("launchable-with")):
            self.fail()
            return self.inform("launch-off.failed")
        self.inform("launch-off")

