# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action3, Action2
from mud.events import LaunchWithEvent, LaunchOnEvent, LaunchOffEvent

class LaunchWithAction(Action3):
    EVENT = LaunchWithEvent
    ACTION = "launch-with"
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"

class LaunchOnAction(Action2):
    EVENT = LaunchOnEvent
    ACTION = "launch-on"
    RESOLVE_OBJECT = "resolve_for_operate"

class LaunchOffAction(Action2):
    EVENT = LaunchOffEvent
    ACTION = "launch-off"
    RESOLVE_OBJECT = "resolve_for_operate"
